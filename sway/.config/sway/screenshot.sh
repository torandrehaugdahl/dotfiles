#!/bin/bash

which grim 2>&1 >/dev/null
HAS_GRIM=$?

which slurp 2>&1 >/dev/null
HAS_SLURP=$?

which wl-copy 2>&1 >/dev/null
HAS_WLCOPY=$?

if [ $HAS_GRIM == "0" ] && [ $HAS_SLURP == "0" ] && [ $HAS_WLCOPY == "0" ]; then
    grim -g "$(slurp)" - | wl-copy
fi
