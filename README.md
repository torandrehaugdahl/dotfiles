# README

## Zsh
Before stowing the zsh files:
- Install [Oh My Zsh!](https://github.com/ohmyzsh/ohmyzsh)
    - The installer script can be found [here](https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)


## Neovim
The nvim directory contains LUA configuration files for nvim and is the one currently in use.
The nvim-old directory contains the old VIML configuration files.

## i3 WM
- Install `i3-wm` (or `i3-gaps`)
- Install `i3lock`
- Install `rofi` (application launcher)
- Install `polybar`

## Sway WM
- Install `sway`, `waybar`, `swaybg`, `swayidle`, `swaylock`
- Install `fuzzel` (application launcher).

### Run Chromium-based apps in Wayland
```
chromium --enable-features=UseOzonePlatform --ozone-platform=wayland
```

