if not pcall(require, "nvim-treesitter.configs") then
    return
end

local ts = require "nvim-treesitter"
local tsconfig = require "nvim-treesitter.configs"

local parsers = require'nvim-treesitter.parsers'

-- Install the missing parsers
local wanted_parsers = require'tah.treesitter.wanted_parsers'

require'nvim-treesitter.configs'.setup {
    highlight = {
        enable = true,
        disable =  "help"
    },
    ensure_installed = wanted_parsers,
    sync_install = false,
    indent = { enable = true },
    auto_install = true,
    ignore_install = {},
    modules = {}
}


for _,v in ipairs(wanted_parsers.manual) do
    print ( "the parser for ", v, " has to be installed manually" )
end


vim.filetype.add({
    pattern = {
        ['.*/*.mo'] = 'modelica'
    }
})

vim.o.foldmethod = "expr"
vim.o.foldexpr = "nvim_treesitter#foldexpr()"
