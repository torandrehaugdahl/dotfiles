local M = {}

M.wanted = {

    -- Speed
    "cpp",
    "c",
    "rust",
    "zig",

    -- Vim
    "vimdoc",
    "vim",
    "luadoc",
    "lua",

    -- Script
    "typescript",
    "python",
    "dockerfile",

    -- Config
    "toml",
    "yaml",
    "json",
    "ini",

    -- Compilers
    "tablegen",
    "llvm",

    -- Linux
    "bash",
    "awk",

    -- Misc
    "markdown",
    "go",
    "glsl",
    "css"
}

M.manual = {
    "mlir"
}

return M
