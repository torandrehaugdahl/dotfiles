local HOME = vim.uv.os_homedir("HOME")

local determine_configdir = function()
    local OS = vim.uv.os_uname().sysname
    if OS == 'Linux' then
        return HOME .. "/.config/nvim"
    else
        return HOME .. "/AppData/Local/nvim"
    end
end

return determine_configdir()

