-- Basic options
vim.o.number = true
vim.o.relativenumber = true
vim.o.softtabstop = 4
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true
vim.o.smartindent = true

vim.o.updatetime = 50

vim.o.signcolumn = "yes"
vim.o.hlsearch = false

vim.o.hidden = true
vim.o.swapfile = false

vim.o.exrc = true
vim.o.secure = true

vim.o.cursorline = true
vim.o.scrolloff = 8

vim.opt.colorcolumn:append("80")


