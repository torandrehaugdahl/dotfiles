local M = {}

local ok, dapui = pcall(require, 'dapui')

if not ok then
    return {}
end

local dap = {}
ok, dap = pcall(require, 'dap')

if not ok then
    return {}
end

dapui.setup({})

dap.listeners.before.attach.dapui_config = function()
    dapui.open()
end

dap.listeners.before.launch.dapui_config = function()
    dapui.open()
end

dap.listeners.before.event_terminated.dapui_config = function()
    dapui.open()
end

dap.listeners.before.event_exited.dapui_config = function()
    dapui.open()
end

return M
