local M = {}


local mappings = {
    n = {
        ["<leader>db"] = {
            "<cmd> DapToggleBreakpoint <CR>",
            "Add breakpoint at line"
        },
        ["<leader>dr"] = {
            "<cmd> DapContinue <CR>",
            "Start or continue the debugger"
        },
      ["<leader>duc"] = {
        require'dapui'.close,
        "Close dapui"
      }
    }
}

local dapui = require('tah.dap.dapui-config')

local lldb = require('tah.dap.lldb-dap-config')

local mapper = require('tah.core.load-keymap')

mapper.load_keymap(mappings)


return M
