local M = {}

local lldb_path = vim.fn.exepath('lldb-dap')

if lldb_path:len() == 0 then
    return {}
end

local dap  = require('dap')
dap.adapters.lldb = {
    type = 'executable',
    command = lldb_path,
    name = 'lldb'
}

dap.configurations.cpp = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = function()
          local input_args = vim.fn.input("Arguments (space-separated): ")
          return vim.split(input_args, ' ', {trimempty = true})
        end
    }
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp

M = {
}



return M
