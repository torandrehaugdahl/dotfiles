local M = {}


--[[
Example keymap

{
    n = {
        ["<leader>db"] = {
            "<cmd> DapToggleBreakpoint <CR>",
            "Some documentation about the keymap"
        }
    }
}

--]]

local load_mode_keys = function(mode, t)

    for mapping, command in pairs(t) do
        if type(command) == "string" then
            vim.keymap.set(mode, mapping, command, {silent=true, remap=false})
        elseif type(command) == "table" then
            -- Assume options are set here
            vim.keymap.set(mode, mapping, command[1], {silent=true, remap=false})
        else if type(command) == "function" then
            vim.keymap.set(mode, mapping, command, {silent=true, remap=false})
        end
    end
end
end

M.load_keymap = function(t)

    -- Normal Mode
    -- Visual Mode
    -- Visual and Select

    local modes = { 'n', 'i', 'x', 'v', 'o' }

    for _,v in ipairs(modes) do
        -- Try to find the mode in the table
        if t[v] ~= nil then
            load_mode_keys(v, t[v])
        end
    end
end


return M
