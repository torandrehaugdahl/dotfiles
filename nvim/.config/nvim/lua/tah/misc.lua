local M = {}

M.clearWhitespace = function()
    local currentView = vim.fn.winsaveview()
    vim.cmd('keeppatterns %s/\\v\\s+$//e')
    vim.fn.winrestview(currentView)
end

vim.fn.ClearWhitespace = M.clearWhitespace

local id = vim.api.nvim_create_augroup("TOR_CLEARWHITESPACE", {clear = true})

vim.api.nvim_create_autocmd({"BufWrite"},{
    pattern = {"*"},
    callback = function(ev)
        M.clearWhitespace()
    end,
    group = id
    }
)


-- vim.cmd[[
-- augroup TOR_CLEARWHITESPACE
--     au!
--     autocmd BufWrite * lua vim.fn.ClearWhitespace()
-- augroup END
-- ]]


if vim.fn.executable("rg") then
    vim.o.grepprg = 'rg --vimgrep --no-heading'
    vim.o.grepformat = '%f:%l:%c:%m'
end

M.c_headerguard = function()
    local bufname = vim.fn.bufname('%')
    bufname = bufname:gsub("%.", "_")
    bufname = bufname:upper()
    bufname = bufname:gsub(".+/", "")

    print(bufname)

    vim.fn.execute("normal i#ifndef _" .. bufname .. "_")
    vim.fn.execute("normal o#define _" .. bufname .. "_")
    vim.fn.execute("normal o#endif")
    vim.fn.execute("normal O")

end

vim.fn["C_HeaderGuard"] = M.c_headerguard

local draw_box = function(str)
    local padding = 2
    local corners = {'┌', '┐', '└', '┘'}
    local borders = { '─', '│' }


    local ypos = vim.api.nvim_win_get_cursor(0)[2]

    local content_width = string.len(str)

    local width_borders = string.rep(borders[1], padding * 2 + content_width)

    local top = corners[1] .. width_borders .. corners[2]
    local content = borders[2] .. string.rep(" ", padding) .. str .. string.rep(" ", padding) .. borders[2]
    local bottom = corners[3] .. width_borders  .. corners[4]

    -- Insert three lines
    vim.api.nvim_buf_set_lines(0, ypos - 1, ypos - 1, false, {top, content, bottom})

end

M.draw_box = draw_box

vim.fn["DrawBox"] = M.draw_box

vim.api.nvim_create_user_command('DrawBox', function(opts)
    M.draw_box(opts.args)
end, { nargs = 1 })

vim.cmd[[
augroup TOR_C
    au!
    au BufNewFile,BufAdd *.c,*.cpp,*.h,*.hpp,*.hh,*.cc nnoremap <leader>cchg :lua vim.fn.C_HeaderGuard()<CR>
augroup END
]]

-- Quickfix list populated by rg
vim.cmd[[
    nnoremap <leader>ss :cgete system('rg --vimgrep ' . input("Input search term: "))<CR>
]]


