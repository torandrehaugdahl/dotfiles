local M = {}


--[[
Usage:
nmap("[keycombo]", callback, { settings })

Example:
nmap({"<leader>vrn", vim.lsp.buf.rename, { remap=false              } })
--]]
M.imap = function(tbl)
	vim.keymap.set("i", tbl[1], tbl[2], tbl[3])
end

M.nmap = function(tbl)
    vim.keymap.set("n", tbl[1], tbl[2], tbl[3])
end

M.vmap = function(...)
    vim.keymap.set("v", unpack({...}))
end

return M
