local M = {}

-- Easy running
M.parse_runcom = function(commands)
    local command_table = {}

    for line in commands:gmatch("[^\r\n]+") do
        -- Skip empty lines
        if line:match("%S") then
            -- Split on the first hyphen and trim whitespace
            local key, value = line:match("^%s*(%S+)%s*-%s*(.+)%s*$")
            if key and value then
                command_table[key:lower()] = value
            end
        end
    end

    return command_table
end

M.run_project = function(args)

    local filename = vim.api.nvim_buf_get_name(0)
    local dir = vim.fs.dirname(filename)

    local trailing_dir = nil

    local target = "run"

    if args ~= nil and string.len(args) > 0 then
        target = args
    end

    local found = 0
    local command_table = {}

    -- Go up until a .runcom file is encountered
    while dir ~= trailing_dir do
        local runcom_path = dir .. "/.runcom"
        local f = io.open(runcom_path, "r")

        if f then
            runcommand = f:read("*all")

            command_table = M.parse_runcom(runcommand)

            f:close()

            found = 1
            goto exit
        end

        trailing_dir = dir
        dir = vim.fs.dirname(dir)
    end
    ::exit::

    -- TODO: Check if further up than cwd
    if found then
        if command_table[target] ~= nil then

            local invocation = command_table[target]

            invocation:gsub("%%", filename)

            vim.cmd("!" .. invocation)
        else
            print("Target doesn't exist.")
        end
    else
        print("Could not find .runcom in your project root!")
    end
end

vim.api.nvim_create_user_command("TRUN", function(opts)
    local args = opts["args"]
    M.run_project(args)
end, {nargs="*"})


local mapper = require 'tah.keymap'

mapper.nmap( {"<leader><F5>", function() M.run_project("run") end, {noremap=true} })
mapper.nmap( {"<leader><F6>", function() M.run_project("test") end, {noremap=true} })


