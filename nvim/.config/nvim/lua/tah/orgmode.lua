if ( true)  then return end
local keymap = require'tah.keymap'
require'orgmode'.setup_ts_grammar()

local HOME = os.getenv('HOME')


local M = {}

M.root_dir = os.getenv('TAH_ORGMODE_DIR') or
                HOME .. "/ORGMODE/"

M.agenda_dir = M.root_dir

require('nvim-treesitter.configs').setup {
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = {'org'},
    },
    ensure_installed = {'org'},
}


require'orgmode'.setup({
    org_agenda_files = {'~/ORGMODE/*'},
    org_default_notes_file = '~/ORGMODE/refile.org',
    org_todo_keywords = {'TODO(t)', 'PROJECT(p)', 'ACTIVE(a)', 'WAITING(w)', '|', 'DONE(d)', 'COMPLETE(c)', 'CANCELED(C)'}

})

------------------------------------------------------------
--               USER SPECIFIC CONFIGURATION              --
------------------------------------------------------------

-- Ensure telescope is loaded
local has_telescope, _ = pcall(require, 'telescope')


-- Check for existence of orgmode directory using `stat`
local stat_code, _ = pcall(
            os.execute,
            string.format("stat %s > /dev/null", M.root_dir))


if stat_code and has_telescope then

    M.orgfile_picker = function()
        require'telescope.builtin'.find_files({
            search_dirs = {M.root_dir},
            hidden = true
            })
    end

    local glob_pattern = table.concat(vim.tbl_flatten { orgmode_dir, "*.org" } )

    keymap.nmap( {"<leader>OO", M.orgfile_picker, {noremap=true}})
end

local keymap = require 'tah.keymap'


return M
