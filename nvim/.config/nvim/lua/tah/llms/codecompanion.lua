local M = {}

local ok, coco = pcall(require, 'codecompanion')

if not ok then
    return {}
end

--[[
Set up CodeCompanion from
https://github.com/olimorris/codecompanion.nvim#gear-configuration
--]]

coco.setup({
    -- Set the default strategies
    strategies = {
        chat = {
            adapter = "ollama"
        },
        inline = {
            adapter = "ollama"
        }
    },
    adapters = {
        ollama = function()
            return require'codecompanion.adapters'.extend("ollama", {
                schema = {
                    model = {
                        default = "qwen2.5-coder:7b",
                        num_ctx = 4096
                    }
                 }
            })
        end
    }
})

local ok, mapper = pcall(require, 'tah.keymap')


local function open_chat()
    coco.chat()
end

local function actions()
    coco.actions()
end

local function inline()
    vim.api.nvim_command("CodeCompanion")
end

if ok then
-- nmap({"<leader>vrn", vim.lsp.buf.rename, { remap=false              } })
    mapper.nmap({"<leader>coc", open_chat, {silent = true, remap = false}})
    mapper.nmap({"<leader>coi", inline, {silent = true, remap = false}})
    mapper.vmap("<leader>coa", actions, {silent = true, remap = false})
end
