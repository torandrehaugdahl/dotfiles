local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(
{
    {
            'nvim-lualine/lualine.nvim',
            dependencies = { 'kyazdani42/nvim-web-devicons', lazy = true}
    },

    'neovim/nvim-lspconfig',
    'junegunn/vim-easy-align',

    'mfussenegger/nvim-dap',
    { "rcarriga/nvim-dap-ui", dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"} },
    --'tpope/vim-surround',

    'lervag/vimtex',

    -- 'NeogitOrg/neogit',

    'mfussenegger/nvim-jdtls', name='jdtls',

    'nvim-tree/nvim-tree.lua',

    --" Emmet
    'mattn/emmet-vim',


    --" Completion
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/vim-vsnip',
    'hrsh7th/cmp-buffer',
    { 'onsails/lspkind.nvim' },

    {
        "olimorris/codecompanion.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter",
            "hrsh7th/nvim-cmp", -- Optional: For using slash commands and variables in the chat buffer
            "nvim-telescope/telescope.nvim", -- Optional: For using slash commands
            { "stevearc/dressing.nvim", opts = {} }, -- Optional: Improves `vim.ui.select`
        },
        config = true
    },

    -- " Snippet Engine
    'SirVer/ultisnips',

    --" Snippets
    'honza/vim-snippets',
    'quangnguyen30192/cmp-nvim-ultisnips',


    -- Color Themes
	--" Plug 'morhetz/gruvbox'
    --" Plug 'dracula/vim', { 'as': 'dracula.vim' }
    --" Plug 'ayu-theme/ayu-vim'
    'folke/tokyonight.nvim',
    'joshdick/onedark.vim',

    --" Treesitter
    'nvim-treesitter/nvim-treesitter',
    'nvim-treesitter/playground',

    -- " Telescope
    'nvim-lua/popup.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope.nvim',

    -- { 'nvim-orgmode/orgmode', config = function()
    --     require('orgmode').setup{}
    -- end
    -- }


}, {})

require'codecompanion'.setup({
  adapters = {
    ollama = function()
      return require("codecompanion.adapters").extend("ollama", {
        headers = {
          ["Content-Type"] = "application/json",
        },
        parameters = {
          sync = true,
        },
        schema = {
            model = {
                default = "mistral"
                }
            },
      strategies = {
        chat = {
          adapter = "anthropic",
        },
        inline = {
          adapter = "copilot",
        },
        agent = {
          adapter = "anthropic",
        },
      },
      })

    end,
  },
})
