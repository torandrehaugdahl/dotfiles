local nmap, imap = require'tah.keymap'.nmap, require'tah.keymap'.imap

vim.o.completeopt = "noinsert,menuone,noselect"

local lsp = require "lspconfig"

local M = {}

lsp.clangd.setup {
    cmd = {
        "clangd",
        "--background-index",
        "--suggest-missing-includes",
        "--compile-commands-dir=build"
    },
    filetypes = { 'c', 'cpp', 'objc', 'objcpp', 'cuda' },
}

--[[
```sh
# Get standard library source for compiler frontend
rustup component add rust-src
# Get the language server
rustup component add rust-analyzer
```
--]]
lsp.rust_analyzer.setup({
    settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true
            },
        }
    }
})


lsp.texlab.setup {}

vim.cmd[[
augroup TOR_LATEX
    au!
    au BufEnter *.tex,*.sty set ft=tex
augroup END
]]

lsp.psalm.setup{}

lsp.bashls.setup{}

--lsp.jdtls.setup{}

--lsp.metals.setup{}


-- https://github.com/hashicorp/terraform-ls/blob/main/docs/installation.md
lsp.terraformls.setup{}

lsp.hls.setup{}

lsp.vuels.setup{}

--[[
```
git clone https://github.com/zigtools/zls
cd zls
zig build -Doptimize=ReleaseSafe
```
--]]
lsp.zls.setup{}

-- `pip install cmake-language-server`
lsp.cmake.setup{}

-- `npm install -g typescript typescript-language-server`
lsp.ts_ls.setup{}

--[[
```sh
npm i -g vscode-langservers-extracted
```
]]--
lsp.cssls.setup{}


--[[

MLIR LSP Server and TableGen LSP Server are part of
llvmk

--]]
lsp.mlir_lsp_server.setup{}
lsp.tblgen_lsp_server.setup{}

-- `go install golang.org/x/tools/gopls@latest`
lsp.gopls.setup{}

---[[
lsp.ltex.setup{
    settings = {
        ltex = {
            language = "en-US",
        },
    },
}
--]]

--[[
    Lua Language Server

    ## Package Manager Install
    Install `lua-language-server` using `pacman` or your distro's package manager.

    ## From source
    See https://github.com/LuaLS/lua-language-server/releases/tag/3.9.3
]]--
lsp.lua_ls.setup {
    settings = {
        Lua = {
            runtime = {
                version = 'LuaJIT',
            },
            diagnostics = {
                globals = {
                    'vim',
                    'require'
                },
            },
            workspace = {
                library = vim.api.nvim_get_runtime_file("", true)
            },
            telemetry = {
                enable = false
            },
        }
    },
}

lsp.pylsp.setup {}


------------------------------------------
-- Language Server Interactions         --
------------------------------------------

local find_references = function()
    local ok, res = pcall(require, 'telescope.builtin')

    -- Revert to default selector
    if not ok then
        vim.lsp.buf.references()
    end

    res.lsp_references()
end

local find_implementations = function()
    local ok, res = pcall(require, 'telescope.builtin')

    if not ok then
        vim.lsp.buf.implementation()
        return
    end

    res.lsp_implementations()

end

M.find_references = find_references
M.find_implementations = find_implementations

local doc_symbol = function()
    local ok, res = pcall(require, 'telescope.builtin')

    if not ok then
        vim.lsp.buf.document_symbol()
        return
    end


    res.lsp_document_symbols()
end


M.reload_all_clients = function()
    local active = vim.lsp.get_clients()

    -- Janky short-term fix for when the LSP fails due to inconsistent state
    local workspace_dirs = vim.uv.os_homedir('HOME') .. "/.cache/workspace/*"

    local files = vim.fn.glob(workspace_dirs, false, true)

    for _, file in ipairs(files) do
        local ok = vim.fn.delete(file, "d")

        if not ok then
            print("Failed to delete " .. file)
        end
    end

    vim.lsp.stop_client(active)
end

vim.api.nvim_create_user_command('ReloadAllClients', function(opts)
    M.reload_all_clients()
end, {nargs = 0} )


nmap({"gd",          vim.lsp.buf.definition,     { remap=false, silent=true } })
nmap({"gr",          M.find_references,          { remap=false, silent=true } })
nmap({"gi",          M.find_implementations,     { remap=false, silent=true } })
nmap({"<C-k>",       vim.lsp.buf.hover,          { remap=false, silent=true } })
imap({"<C-k>",       vim.lsp.buf.signature_help, { remap=false, silent=true } })
nmap({"<leader>vh",  vim.lsp.buf.hover,          { remap=false              } })
nmap({"<leader>vrn", vim.lsp.buf.rename,         { remap=false              } })
nmap({"<leader>vsd", vim.diagnostic.open_float,  { remap=false              } })
nmap({"<leader>nd",  function() vim.diagnostic.jump( { count=1, float=true} ) end,    { remap=false } })
nmap({"<leader>Nd",  function() vim.diagnostic.jump( { count=-1, float = true} ) end, { remap=false } })
nmap({"<leader>ca",  vim.lsp.buf.code_action,    { remap=false, silent=true } })
nmap({"<leader><C-o>", doc_symbol, { remap=false, silent=true} })
