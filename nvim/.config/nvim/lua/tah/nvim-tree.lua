-- from https://github.com/nvim-tree/nvim-tree.lua
-- vim.g.loaded_netrw = 1
-- vim.g.loaded_netrwPlugin = 1

vim.opt.termguicolors = true

local M = { }


local ok, tree = pcall(require, "nvim-tree")

if not ok then
    return
end

-- Bind tree to key press

require'tah.keymap'.nmap({
    '<leader>ee',
    function()
        vim.cmd(":NvimTreeOpen")
    end,
    {silent = true}})

tree.setup({
  sort = {
    sorter = "case_sensitive",
  },
  view = {
    width = 30,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})
