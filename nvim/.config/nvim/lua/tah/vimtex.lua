local nmap, imap = require'tah.keymap'.nmap, require'tah.keymap'.imap


vim.g.vimtex_view_method = "zathura"

local aug = vim.api.nvim_create_augroup("TAHVimtex", { clear = true } )

vim.api.nvim_create_autocmd(
    {"BufEnter", "BufWinEnter"},
    {
        pattern = { "*.tex" },
        callback = function(ev)
            vim.keymap.set('n', '<leader>gt', function()
                vim.cmd":VimtexView"
            end, {
            buffer = true,
            silent = true,
            })
        end,
        group = aug
    }
)


vim.api.nvim_create_autocmd(
    {"BufEnter", "BufWinEnter"},
    {
        pattern = { "*.tex" },
        callback = function(ev)
            vim.keymap.set('n', '<leader>vc', function()
                vim.cmd":VimtexCompile"
            end, {
            buffer = true,
            })
        end,
        group = aug
    }
)
