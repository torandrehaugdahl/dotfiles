local M = {}

M.project_files = function()
        local opts = {}
        local ok = pcall(require'telescope.builtin'.git_files, opts)
        if not ok then require'telescope.builtin'.find_files(opts) end
end

M.local_files = function()
    local opts = {}
    require'telescope.builtin'.find_files(opts)
end

local HOME = os.getenv('HOME')

M.config_files = function()
    require'telescope.builtin'.find_files(
        {
            search_dirs = {HOME .. "/.config/nvim/lua/", HOME .. "/.config/nvim/ftplugin/"},
            hidden=true
        })
end

local nmap = require'tah.keymap'.nmap

nmap({"<C-p>", M.project_files, {noremap=true}})
nmap({"<leader><C-p>", M.local_files, {noremap=true}})
nmap({"<leader>ed", M.config_files, {noremap=true}})
