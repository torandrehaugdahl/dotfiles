local M = {}

TAH_CPPCHECK_NS = vim.api.nvim_create_namespace('cppcheck')


M.imap = function(tbl)
	vim.keymap.set("i", tbl[1], tbl[2], tbl[3])
end

M.nmap = function(tbl)
	vim.keymap.set("n", tbl[1], tbl[2], tbl[3])
end

vim.bo.matchpairs = vim.bo.matchpairs .. ',<:>'


local cppcheck = function()
    local current_file = vim.fn.expand('%:p')

    vim.notify(current_file)

    local cmd = {'cppcheck', '--enable=all', '--suppress=missingIncludeSystem', current_file}

    local ns_id = vim.api.nvim_create_namespace('cppcheck')


    local result = vim.system(cmd):wait()

    local buffer_number = vim.fn.bufnr('%')

    local tbl = {}

    for line in string.gmatch(result.stderr, "([^\n]*)") do
        --vim.notify(line .. "\n")

        for fn,ln,cn,txt in string.gmatch(line, "([^:]+):([%d]+):([%d]+):(.+)") do
            table.insert(tbl, {filename=fn, lnum=ln, col=cn, text=txt})

            local opts = {
                end_line = tonumber(ln),
                virt_text = {{txt, "IncSearch"}},
                virt_text_pos = 'eol'
            }

            vim.api.nvim_buf_set_extmark(buffer_number, TAH_CPPCHECK_NS, tonumber(ln), 0, opts)
        end
    end

    vim.fn.setqflist(tbl)
    vim.cmd("copen")


end


M.nmap({"<F8>", cppcheck, {remap=false, silent=true}})


return M
