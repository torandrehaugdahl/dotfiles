--------------------------------------------------------------------------------
--                                  INIT.LUA                                  --
--                 This is my init.lua config file for neovim                 --
--------------------------------------------------------------------------------


-- !!! Check out tah.global for configuration
-- Disable patched fonts if not using nerd fonts.

local HOME = vim.uv.os_homedir("HOME")


local CONFIGDIR = require'tah.configdir'

vim.g.mapleader = '-'

local mapper = require "tah.keymap"

-- Basic mappings
mapper.imap({"jk", "<ESC>", {noremap = true}})
mapper.nmap({"<leader>ev", ":vs " .. CONFIGDIR .. "/init.lua<CR>", {noremap=true} } )
mapper.nmap({"<leader>sv", ":source " .. CONFIGDIR .. "/init.lua<CR>", {noremap=true} } )

-- Basic Configuration
require "tah.profile"
require "tah.plugins"
require "tah.telescope"
require "tah.lsp"
require "tah.nvim-tree"
require "tah.completion"
require "tah.lualine"
require "tah.treesitter.treesitter"
require "tah.misc"
require "tah.orgmode"
require "tah.vimtex"
require "tah.neogit"
require "tah.dap.dap-config"

-- LLM support
require "tah.llms.codecompanion"

vim.cmd.colorscheme('tokyonight-moon')
vim.o.background = "dark"

vim.filetype.add({
  extension = {
    mlir = "mlir",
  },
})
