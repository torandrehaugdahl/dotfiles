;; Don't show the splash screen

(setq inhibit-startup-message t)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-display-line-numbers-mode 1)

(load-theme 'tsdh-dark t)

(hl-line-mode 1)

(setq tah-config-location
      (expand-file-name "init.el"
			      (or (getenv "XDG_CONFIG_HOME")
			      "~/.emacs.d" ) )  )

(defun tah-open-config ()
  "Opens my configuration file."
  (interactive)
  (let ((newbuf (find-file-noselect tah-config-location t)))
	(switch-to-buffer-other-window newbuf)
))

(defun tah-write-agenda ()
  "Writes the agenda to pdf."
  (interactive)
  (progn
    (org-agenda-list)
    (org-agenda-write "~/agenda.pdf")
    )
  )

;; (global-set-key "- e v"
(add-to-list 'default-frame-alist
                       '(font . "PragmataPro Mono"))
;; Evil disabled to get familiar with basic emacs movements
(require 'package)
;;(require 'use-package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
 ;; (package-refresh-contents)

 ;; Download EVIL
; (unless (package-installed-p 'evil)
;   (package-install 'evil))

;; Download Helm
(unless (package-installed-p 'helm)
  (package-install 'helm))

(unless (package-installed-p 'org)
  (package-install 'org))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
;; Enable evil
;(require 'evil)
;;(evil-mode 1)


(defun tah/org-mode-setup ()
;;  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))


(use-package org
    :hook (org-mode . tah/org-mode-setup)
    :config
    (progn
        (setq org-agenda-files '("~/org" "~/org/cag/" "~/org/personal" "~/org/dutywork" "~/org/phd"))
        (setq org-log-done 'time)
        (setq org-log-into-drawer t)
        (setq org-agenda-start-with-log-mode t)
	(setq org-duration-format (quote h:mm))
	(setq org-default-notes-file (expand-file-name "refile.org" "~/org/"))
	(setq org-refile-targets '(
				   (nil :level . 1) (nil :level . 2) (nil :level . 3)
				   ("~/org/todo.org" :maxlevel . 5)
				   (org-agenda-files :tag . "capture")
				   )) ;; End of refile targets
	(setq org-todo-keywords '( (sequence "TODO(t)" "|" "DONE(d)")
				   (sequence "PROJECT(p)" "ACTIVE(a)" "WAITING(w)" "DELEGATED(D)" "|" "COMPLETE(c)")
				   (sequence "|" "CANCELED(C)")
				   (sequence "MAYBE(m)" "|")))
	(setq org-todo-keyword-faces '(
				       ("CANCELED" . (:inherit mode-line :background "red" :foreground "white" :weight bold))
				       ("WAITING" . (:inherit mode-line :foreground "white" :background "#333333" :weight italic))
				       ("ACTIVE" . (:inherit mode-line :foreground "yellow"))
				       ))
	;; Use, e.g., dunst to provide notification support
	(setq org-clock-sound (expand-file-name "emacs-clock.wav" "~/Music/"))
	;; Automatically add tasks to daily note
	(setq org-capture-templates '(("t" "Todo" entry (file+headline "~/org/refile.org" "Tasks")
				       "* TODO %?\n  %i\n  %a")
				      ("j" "Journal" entry (file+datetree "~/org/daily.org")
				       "* %?\n Entered on %U\n  %i\n  %a")))
	(require 'ox-md nil t)
    (setq org-hide-emphasis-markers t)
	)
    )

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)



;; Quickly switch to config directory with `dired`
(global-set-key (kbd "C-c C-p")
		(lambda ()
		  (interactive)
		  (dired "~/.emacs.d/")))

;; Quickly switch to orgmode directory
(global-set-key (kbd "C-c C-o")
		(lambda ()
		  (interactive)
		  (dired "~/org/")))

(setq custom-file "~/.emacs.d/customfile.el")

;; Use helm to
(when (package-installed-p 'helm)
  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (helm-mode 1))

(unless (package-installed-p 'kkp)
  (package-install 'kkp))

(when (package-installed-p 'kkp)
  (use-package kkp
    :ensure t
    :config
    (global-kkp-mode +1))
  )
