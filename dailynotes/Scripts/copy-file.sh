#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "No file supplied"
    exit
fi

cat $1 | xclip -selection clipboard
