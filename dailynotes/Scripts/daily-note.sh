#!/bin/bash

DATEFMT="%Y%m%d"

TODAY=$(date +${DATEFMT})
echo $TODAY

USEDATE=

TIMESTAMP=$(date +%s)
USETIMESTAMP=$TIMESTAMP

NOTES_DIR=$HOME/.dailynotes

if [[ ! -d $NOTES_DIR ]]; then
    echo NOTES DIR DOES NOT EXIST
    echo The application will try to create one

    if mkdir $NOTES_DIR; then
        echo "Success"
    fi
fi


if [ "$#" -gt "0" ]; then
    USETIMESTAMP=$(($TIMESTAMP - $1 * 24 * 60 * 60))
fi

USEDATE=$(date --date="@$USETIMESTAMP" +${DATEFMT})


FILENAME=$NOTES_DIR/${USEDATE}.md

if [[ -z $EDITOR ]]; then
    # Check for existence of vim
    if which vim 2>&1 >/dev/null; then
        EDITOR=vim
    fi
fi

if [[ -z $EDITOR ]]; then
    echo "Couldn't find a suitable editor. Please specify one with the EDITOR env variable"
fi

$EDITOR $FILENAME
