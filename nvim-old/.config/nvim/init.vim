call plug#begin()

	Plug 'vim-airline/vim-airline'
    Plug 'neovim/nvim-lspconfig'
    Plug 'junegunn/vim-easy-align'
    Plug 'petRUShka/vim-opencl'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-fugitive'
    Plug 'tikhomirov/vim-glsl'
    " Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }


    " Emmet
    " Plug 'mattn/emmet-vim'


    " Completion
    Plug 'hrsh7th/nvim-cmp'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/vim-vsnip'
    Plug 'hrsh7th/cmp-buffer'

    " Color Previews
    " Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }

    " Snippet Engine
    Plug 'SirVer/ultisnips'

    " Snippets
    Plug 'honza/vim-snippets'
    Plug 'quangnguyen30192/cmp-nvim-ultisnips'


    " Color Themes
	" Plug 'morhetz/gruvbox'
    " Plug 'dracula/vim', { 'as': 'dracula.vim' }
    " Plug 'ayu-theme/ayu-vim'
    Plug 'joshdick/onedark.vim'

    " Treesitter
    Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
    Plug 'nvim-treesitter/playground'

    " Telescope
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'

    " Plug 'rhysd/vim-clang-format'

call plug#end()

inoremap jk <ESC>

let mapleader='-'

nnoremap <leader>sv :source ~/.config/nvim/init.vim<CR>
nnoremap <leader>ev :vs ~/.config/nvim/init.vim<CR>


" Enable navigation in manuals
nnoremap <leader><C-]> <C-]>
nnoremap <leader>rr :set rnu!<CR>

syntax enable
filetype plugin on

set softtabstop=4
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Quicker Update Time
set updatetime=50

set number
set relativenumber
set signcolumn=yes
set nohlsearch

set hidden
set noswapfile

" Allow for project-specific vimrc
set ex
set secure

" Highlight current line
set cursorline
set scrolloff=8

" Highlight 80th column
set colorcolumn+=80

function! C_HeaderGuard()
    let l:bufn = bufname('%')
    let l:bufn = substitute(l:bufn, '\.', '_', '')
    let l:bufn = toupper(l:bufn)
    let l:bufn = substitute(l:bufn, '\v(.+/)+', '', '')

    echo l:bufn

    execute "normal i#ifndef _".l:bufn."_\<ESC>o"
    execute "normal i#define _".l:bufn."_\<ESC>o"
    execute "normal i#endif\<ESC>O"
endfunction

augroup TOR_C
    " Clear the autocmds of this group
    au!
    au BufNewFile,BufAdd *.c,*.cpp,*.h,*.hpp,*.hh,*.cc nnoremap <leader>cchg :call C_HeaderGuard()<CR>
augroup END
