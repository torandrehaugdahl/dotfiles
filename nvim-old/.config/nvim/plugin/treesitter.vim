" Treesitter Configuration
if &rtp =~ "treesitter"
    lua require'nvim-treesitter.configs'.setup { highlight = { enable = true } }

    set foldmethod=expr
    set foldexpr=nvim_treesitter#foldexpr()

    " augroup TOR_TREESITTER
    "     au!
    "     au BufNew *.cpp,*.c,*.hpp,*.h,*.cu set foldmethod=expr
    "     au BufNew *.cpp,*.c,*.hpp,*.h,*.cu set foldexpr=nvim_treesitter#foldexpr()
    "     au BufNew *.cpp,*.c,*.hpp,*.h,*.cu echomsg "Folding"
    " augroup END
endif
