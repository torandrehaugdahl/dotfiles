" ============================================================
" Automatically Clear Whitespace
" ============================================================
function! ClearWhitespace()
    let l:curview = winsaveview()
    keeppatterns %s/\v\s+$//e
    call winrestview(l:curview)
endfunction

augroup TOR
    autocmd BufWrite * call ClearWhitespace()
augroup END

" ============================================================
" Moving window between tabs
" ============================================================
function! MoveToNextTab()
    if tabpagenr('$') == 1 && winnr('$') == 1
        " Only one buffer open
        return
    endif

    let l:cur_buf = bufnr('%')
    let l:tabnum = tabpagenr('$')

    if tabpagenr() < l:tabnum
        close!
        tabnext
        vsplit
    else
        close!
        tabnew
    endif
    exe "b".l:cur_buf
endfunction

function! MoveToPrevTab()
    if tabpagenr('$') == 1 && winnr('$') == 1
        return
    endif

    let l:cur_buf = bufnr('%')
    let l:tabnum = tabpagenr()

    if tabpagenr() != 1
        close!
        tabprev
        vsplit
    else
        close!
        exec "0tabnew"
    endif
    exe "b".l:cur_buf
endfunction


" Fast Pane Resize
nnoremap <M-,> :call MoveToPrevTab()<CR>
nnoremap <M-.> :call MoveToNextTab()<CR>

nnoremap <M-h> :tabprev<CR>
nnoremap <M-l> :tabnext<CR>

" Fast Resize
let g:usr_smallres=5
nnoremap <leader>l :execute "vertical resize +".g:usr_smallres<CR>
nnoremap <leader>h :execute "vertical resize -".g:usr_smallres<CR>
nnoremap <leader>k :execute "resize +".g:usr_smallres<CR>
nnoremap <leader>j :execute "resize -".g:usr_smallres<CR>

let g:usr_bigres =40

nnoremap <leader><S-l> :execute "vertical resize +".g:usr_bigres<CR>
nnoremap <leader><S-h> :execute "vertical resize -".g:usr_bigres<CR>
nnoremap <leader><S-k> :execute "resize +".g:usr_bigres<CR>
nnoremap <leader><S-j> :execute "resize -".g:usr_bigres<CR>

