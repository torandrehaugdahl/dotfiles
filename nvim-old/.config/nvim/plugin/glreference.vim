" OpenGL Reference Opener


if exists("g:cvr_glref_loaded") || &cp || v:version < 700
    finish
endif

if !exists("g:cvr_glref_open_command")
    if has("win32")
        let g:cvr_glref_open_command="start"
    elseif substitute(system("uname"), "\n", "", "") == "Darwin"
        let g:cvr_glref_open_command="open"
    else
        let g:cvr_glref_open_command="xdg-open"
    endif
endif

if !exists("g:cvr_glref_command")
    let g:cvr_glref_command="GLR"
endif

if !exists("g:cvr_glref_khronos_prefix")
    let g:cvr_glref_khronos_prefix = "https://khronos.org/registry/OpenGL-Refpages/gl4/html/"
endif

if ! exists("g:cvr_glref_krhonos_suffix")
    let g:cvr_glref_khronos_suffix = ".xhtml"
endif


function s:glref()
    let functionString = expand("<cword>")

    let functionName = matchstr(functionString, "gl[a-zA-Z]*")

    if empty(functionName)
        echohl Error
        echomsg "word under cursor is not an OpenGL function"
        echohl None
        return
    endif

    if has("win32")
        silent! execute "! " . g:cvr_glref_open_command . " \"\" \"" . g:cvr_glref_khronos_prefix . functionName . g:cvr_glref_krhonos_suffix
    else
       silent! execute "! " . g:cvr_search_open_command . ' "' . g:cvr_glref_khronos_prefix . functionName . g:cvr_glref_khronos_suffix . '" >/dev/null 2>&1 &'
    endif

endfunction

execute "command! -nargs=* -range " . g:cvr_glref_command . " :call s:glref()"
