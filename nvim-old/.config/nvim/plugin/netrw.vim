" ============================================================
" Netrw Config
" ============================================================
let g:netrw_liststyle=3
let g:netrw_winsize=30
let g:netrw_browse_split=4
nnoremap <leader>ve :Vex<CR>

