" ============================================================
" NEOVIM LSP
" ============================================================
set completeopt=noinsert,menuone,noselect

" Prevent vanilla vim from processing this file
if ! has("nvim")
    finish
endif

" Clangd
let s:has_clangd = system('which clangd')
if (! v:shell_error)
    lua << EOF
    require'lspconfig'.clangd.setup{
        filetypes = { 'c', 'cpp', 'objc', 'objcpp', 'cuda' }
    }
EOF
endif

" Rust Language Server
let s:has_rls = system('which rls')
if ( ! v:shell_error )
    lua << EOF
    require'lspconfig'.rls.setup{
   	settings = {
                unstable_features = true,
                build_on_save = false,
                all_features = true
	}
    }
EOF
endif


" TexLab -- LaTeX Language Server
let has_texlab = system('which texlab')
if ( ! v:shell_error )
    lua require'lspconfig'.texlab.setup{ }

    let g:texlab="yes"

    augroup TOR_LATEX
        au!
        au BufEnter *.tex,*.sty set ft=tex
    augroup END
endif

" Psalm PHP Language Server
let has_psalm = system("which psalm-language-server")
if ( ! v:shell_error )
    lua require'lspconfig'.psalm.setup{}
endif

" Bash Language Server
let has_bashls = system('which bash-language-server')
if ( ! v:shell_error )
    lua require'lspconfig'.bashls.setup{}
endif

" Scala Metals
let has_metals = system('which metals')
if( ! v:shell_error )
    lua require'lspconfig'.metals.setup{}
endif


let has_hls = system('which haskell-language-server-wrapper')
if ( ! v:shell_error )
    lua require'lspconfig'.hls.setup{}
endif

let has_pyls = system('which pylsp')
if ( ! v:shell_error )
    lua require'lspconfig'.pylsp.setup{}
endif

" Installed with pip
" pip install cmake-language-server
" Link: https://github.com/regen100/cmake-language-server
let has_cmakels = system('which cmake-language-server')
if ( ! v:shell_error )
    lua require'lspconfig'.cmake.setup{}
endif

let has_vuels = system('which vls')
if ( ! v:shell_error )
    lua require'lspconfig'.vuels.setup{}
endif


let has_vuels = system('which zls')
if ( ! v:shell_error )
    lua require'lspconfig'.zls.setup{}
endif



nnoremap <silent> gd :lua vim.lsp.buf.definition()<CR>
nnoremap <silent> <C-k> :lua vim.lsp.buf.hover()<CR>
inoremap <silent> <C-k> <ESC>:lua vim.lsp.buf.signature_help()<CR>a
nnoremap <leader>vh :lua vim.lsp.buf.hover()<CR>
nnoremap <leader>vrn :lua vim.lsp.buf.rename()<CR>
nnoremap <leader>vsd :lua vim.diagnostic.open_float()<CR>
nnoremap <leader>dn :lua vim.diagnostic.goto_next()<CR>

let g:completion_matching_strategy_list = ['exact', 'fuzzy', 'substring', 'all']

" ============================================================
" nvim-cmp configuration
" -------------------------------------------------------------
lua <<EOF
  local cmp = require'cmp'
  cmp.setup({
    snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
      end,
    },
    mapping = {
        ['<C-y>'] = cmp.mapping.confirm({ select = true }),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
         }),
        ['<C-n>'] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            else
                fallback()
            end
        end,
        ['<C-p>'] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            else
                fallback()
            end
        end
    },
    sources = {
      { name = 'nvim_lsp', max_item_count = 10000 },
      { name = 'buffer' },
      { name = 'ultisnips' },
      ...
    },
    enabled = function()
        return vim.fn.reg_recording() == '' -- see :h reg_recording
                                            -- disables cmp when vim is currently recording a macro
    end,
  })

EOF

" ============================================================
" Quick Opener for nvim-lspconfig
" ------------------------------------------------------------
" Enables the user to quickly open an lspconfig file by calling
" this function through a command or through a keybind
" ------------------------------------------------------------
function! s:userconf_explore_lspconfig()

    if &ft != "vim"
        echohl Error
        echomsg "Not a vim file"
        echohl None
        return
    endif

    " NOTE: Not supported on windows
    if has("win32")
        return
    endif

    " Get the word under the cursor
    let word = expand("<cword>")

    " Specify the LSP Directory
    let lspdir = system("echo $HOME/.config/nvim/plugged/nvim-lspconfig")

    " Trim the string
    let lspdir = trim(lspdir, "\n\r\t")

    " Find lspconfig files
    let findcommand="find " . lspdir . " -iname \"" . word . "*.lua\""
    let file = system(findcommand)

    " Ensure that there is only one file
    let wc = system("echo \"" . file . "\" | wc -l")

    " TODO: Find out why wc -l returns 2 when there is only one
    if wc > 2
        echohl Error
        echomsg "Found more than one LSP config"
        echohl None
        return
    endif

    if wc < 2
        echohl Error
        echomsg "Could not find an LSPConfig file"
        echohl None
        return
    endif

    " Open the LSPConfig file in a vertical split
    execute "vs " . file

endfunction

command! USERCONFExploreLSPConfig :call s:userconf_explore_lspconfig()

augroup USERCONF_LSPEXPLORE
    au!
    au! BufRead,BufEnter *.vim nnoremap <buffer> <leader>gl :USERCONFExploreLSPConfig<CR>
augroup END
