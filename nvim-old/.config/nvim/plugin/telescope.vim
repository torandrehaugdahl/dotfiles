" ============================================================
" Telescope
" ============================================================
nnoremap <C-p> <cmd>lua require'telescope.builtin'.git_files()<CR>
nnoremap <leader><C-p> <cmd>lua require'telescope.builtin'.find_files()<CR>
"nnoremap <C-p> <cmd>lua require'telescope.builtin'.find_files()<CR>
nnoremap <leader>fg <cmd>lua require'telescope.builtin'.live_grep()<CR>
nnoremap <C-s> :Telescope lsp_document_symbols<CR>


" Get a telescope instance to get all custom config files
nnoremap <leader>ed <cmd>lua require'telescope.builtin'.find_files({search_dirs = {"~/.config/nvim/plugin"}, hidden = true})<CR>
