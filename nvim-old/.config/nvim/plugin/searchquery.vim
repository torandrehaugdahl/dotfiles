" Search Query

let g:cvr_search_cmd="Go"

if exists("g:cvr_search_loaded") || &cp || v:version < 700
    finish
endif

if !exists("g:cvr_search_open_command")
    if has("win32")
        let g:cvr_search_open_command="start"
    elseif substitute(system("uname"), "\n", "", "") == "Darwin"
        let g:cvr_search_open_command="open"
    else
        let g:cvr_search_open_command="xdg-open"
    endif
endif

if !exists("g:cvr_search_query_url")
    " TODO: HTTPS?
    let g:cvr_search_query_url="https://google.com/search?q="
endif

" Specifies what name the script should call python by
if !exists("g:cvr_search_python_cmd")
    let g:cvr_search_python_cmd="python"
endif

" Specifies the user-visible command name.
" Default `:{RANGE}Google`
if !exists("g:cvr_search_cmd")
    let g:cvr_search_cmd="Google"
endif

if !exists("g:cvr_search_ft_cmd")
    let g:cvr_search_ft_cmd=g:cvr_search_cmd . "f"
    echo g:cvr_search_ft_cmd
endif


function s:goo(ft, ...)
    let sel=getpos('.') == getpos("'<") ? getline("'<")[getpos("'<")[2] - 1:getpos("'>")[2] -1] : ''

    if a:0 == 0
        let words= [a:ft, empty(sel) ? expand("<cword>") : sel]
    else
        let query = join(a:000, " ")
        let quotes = len(substitute(query, '[^"]', '', 'g'))
        let words = [a:ft, query, sel]

        if quotes > 0 && quotes % 2 != 0
            call add(words, '"')
        endif

        call filter(words, 'len(v:val)')
    endif

    let query = substitute(join(words, " "), '\s*\(.\{-}\)\s*$', '\1', '')
    let query = substitute(query, '"', '\\"', 'g')

    if has("win32")
        silent! execute "! " . g:cvr_search_open_command . " \"\" \"" . g:cvr_search_query_url . query . "\""
    else
        " Explanation:
        " - The first line enters command substitution mode for goo_query
        " - The second line starts the python command string and imports the
        "   urllib library for URL encoding.
        " - The third line has python print out the encoded query string and
        "   finishes command substitution.
        " - The fourth and fifth lines use the `goo_query` variable to call
        "   the open utility specified by `g:cvr_search_open_command`
        " NOTE: All this string escaping is ugly. Any way to fix?
        silent! execute "! goo_query=\"$(" . g:cvr_search_python_cmd .
            \" -c \"import urllib.parse;" .
            \"query=\\\"\\\"\\\"" . query . "\\\"\\\"\\\";" .
            \"print(urllib.parse.quote_plus(query))\")\"" .
            \"&& " . g:cvr_search_open_command . ' "' . g:cvr_search_query_url .
            \"$goo_query" . '" >/dev/null 2>&1 &'
    endif

endfunction

execute "command! -nargs=* -range " . g:cvr_search_cmd . " :call s:goo('', <f-args>)"
execute "command! -nargs=* -range " . g:cvr_search_ft_cmd . " :call s:goo(&ft, <f-args>)"
