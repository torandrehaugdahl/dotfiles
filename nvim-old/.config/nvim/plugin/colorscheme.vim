" Color Schemes

" Dracula
" colorscheme dracula
" set background=dark

" Gruvbox
colorscheme onedark
set background=dark


" Ayu
" colorscheme ayu
" let ayucolor="mirage" " light, mirage, dark

if has('termguicolors')
    set termguicolors
endif

" Set up for transparency
if ( has('unix') )


    " Check for picom
    let g:has_picom = system('which picom')
    let g:has_picom = v:shell_error

    " Check for wayland
    let g:running_wayland = $XDG_SESSION_TYPE == "wayland"

    if ( g:has_picom || g:running_wayland )
        hi Normal guibg=None ctermbg=None
    endif

endif

" The airline theme is set in airline.vim
